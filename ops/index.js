'use strict';

var ops = function () {

  this.crypt      = require('./crypt');
  this.release    = require('./release');
  this.testrunner = require('./testrunner');

};

module.exports = new ops();

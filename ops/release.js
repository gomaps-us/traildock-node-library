'use strict';

var shell     = require('shelljs');
var winston   = require('winston-color');
var fs        = require('fs');
var path      = require('path');

var emptyObjectValues = function(object) {
  for (var key in object) {
    if (object[key] !== null && typeof object[key] === 'object') {
      emptyObjectValues(object[key]);
    } else {
      object[key] = null;
    }
  }
}

module.exports = function (root, name, crypt, dockerRepoOrg) {

  shell.cd(root);

  var packageJs = require(path.resolve(root, 'package.json'));

  var diffIndex = shell.exec('git diff-index HEAD', { silent: true });
  if (diffIndex != '') {
    winston.warn('Looks like you have unstaged changes, get everything committed before you release');
    process.exit(0);
  }

  var defaults = JSON.parse(fs.readFileSync(path.resolve(root, 'config', 'default.json')));
  emptyObjectValues(defaults);
  fs.writeFileSync(path.resolve(root, 'config', 'template.json'), JSON.stringify(defaults, null, 2));
  crypt.encryptConfigs();
  shell.exec('git add config/')
  shell.exec('git commit -m "updating encrypted default config and config template on release"');

  var tags = shell.exec('git tag -l ' + packageJs.version, { silent: true });
  if (tags.stdout.trim() == packageJs.version) {
    winston.info('Deleting existing tag %s', packageJs.version);
    shell.exec('git tag -d ' + packageJs.version);
  }
  winston.info('Creating new release tag %s', packageJs.version);
  shell.exec('git tag ' + packageJs.version);
  winston.info('Building docker images...');
  shell.exec('docker build -t ' + dockerRepoOrg +'/' + name + ':' + packageJs.version  +' -t ' + dockerRepoOrg + '/' + name + ':stable .');
  winston.info('Pushing release to code and container repositories...');
  shell.exec('git push --force origin refs/tags/' + packageJs.version + ':refs/tags/' + packageJs.version);
  shell.exec('docker push ' + dockerRepoOrg + '/' + name + ':' + packageJs.version);
  shell.exec('docker push ' + dockerRepoOrg + '/' + name + ':stable');

};

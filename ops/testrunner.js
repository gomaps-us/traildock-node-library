'use strict';

var Mocha     = require('mocha');
var winston   = require('winston-color');
var glob      = require('glob');
var hashFiles = require('hash-files');
var shell     = require('shelljs');
var fs        = require('fs');
var path      = require('path');
var sleep     = require('sleep');

module.exports = function (root, options) {

  var _this = this;
  var tests = '';
  options = {
    type: options.type ? options.type : 'all',
    limit: options.limit ? options.limit : null,
    timeout: options.timeout ? options.timeout : 50000,
    ui: options.ui ? options.ui : 'bdd',
    containers: options.containers ? options.containers : []
  };

  this.run = function (err, files, opts) {
    var mocha     = new Mocha(opts);
    var callback  = opts.callback ? opts.callback : null;
    if (err) {
      winston.error(err);
      process.exit(1);
    }
    for (var i = 0; i < files.length; i++) {
      mocha.addFile(path.join(root, files[i]));
    }
    mocha.run(function (failures) {
      if (callback) callback(failures);
      process.on('exit', function () {
        process.exit(failures);
      });
    });
  };

  if (options.type == 'unit' || options.type == 'all') {
    winston.info('Running unit tests...');
    tests = 'tests/{,**/}*.test.js'
    if (options.limit) {
      tests = 'tests/{,**/}' + options.limit + '.test.js'
    }
    glob(tests, { cwd: root }, function (err, files) {
      _this.run(err, files, {
        useColors: true,
        ui: options.ui,
        timeout: options.timeout
      });
    });
  }

  if (options.type == 'functional' || options.type == 'all') {
    winston.info('Getting a checksum for our project files to see if we need to rebuild the test image...');
    var files = [
      '.dockerignore',
      'Dockerfile',
      '{,**/}*.js',
      '{,**/}*.json',
      '!tests/{,**/}*'
    ];
    hashFiles({ files: files }, function (err, hash) {
      if (err) {
        winston.error(err);
        process.exit(1);
      }
      shell.cd(root);
      var checksumFile = path.resolve(root, '.checksum');
      var build = true;
      if (fs.existsSync(checksumFile)) {
        if (fs.readFileSync(checksumFile) == hash) {
          build = false;
        }
      }
      fs.writeFileSync(checksumFile, hash);
      var env = process.env;
      env.CRYPTKEY = fs.readFileSync(path.resolve(root, '.cryptkey'), 'utf8')
      if (build) shell.exec('docker-compose -f docker-compose.tests.yml build', { env: env });
      winston.info('Bringing up functional test server...');
      shell.exec('docker-compose -f docker-compose.tests.yml up -d', { env: env });
      sleep.sleep(3);
      tests = 'tests/{,**/}*.test.functional.js';
      if (options.limit) {
        tests = 'tests/{,**/}' + options.limit + '.test.functional.js'
      }
      glob(tests, { cwd: root }, function (err, files) {
        _this.run(err, files, {
          useColors: true,
          ui: options.ui,
          timeout: options.timeout,
          callback: function (failures) {
            if (failures > 0) {
              winston.error("Some functional test failures, getting the docker logs in case there's useful info in there");
              shell.exec('docker logs ' + options.containers[0]);
            }
            winston.info('Removing test containers...');
            options.containers.forEach(function (container) {
              shell.exec('docker rm -f ' + container);
            });

          }
        });
      });
    });
  }

};

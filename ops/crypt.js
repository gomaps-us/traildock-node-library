'use strict';

var path      = require('path');
var fs        = require('fs');
var crypto    = require('crypto');

module.exports = function (root) {

  var _this   = this;
  var hash    = crypto.createHash("sha1");
  _this.key   = process.env.CRYPTKEY ?
                process.env.CRYPTKEY :
                (fs.existsSync(path.resolve(root, '.cryptkey')) ?
                 fs.readFileSync(path.resolve(root, '.cryptkey'), 'utf8') :
                 null
                );
  if (!_this.key) {
    throw Error('No encryption key found, could not continue with any crypt methods');
  }

  this.encryptConfigs = function () {
    fs.readdir(path.resolve(root, 'config'), function(err, items) {
      if (err) throw err;
      items.forEach(function (item) {
        if (item == 'template.json' || item.match(/\.encrypted$/)) return;
        console.log('Encrypting ' + item + '...');
        var cipher = crypto.createCipheriv('aes-256-ctr', _this.key.toString('hex').slice(0, 32), _this.key.toString('hex').slice(0, 16));
        var encrypted = cipher.update(fs.readFileSync(path.resolve(root, 'config', item), 'utf8'), 'utf8', 'hex');
        encrypted += cipher.final('hex');
        fs.writeFileSync(
          path.resolve(root, 'config', item + '.encrypted'),
          encrypted
        );
      });
    });
  };

  this.decryptConfigs = function (auto) {
    if (!auto) auto = false;
    if (auto && fs.existsSync(path.resolve(root, 'config', 'default.json'))) {
      console.log('default.json decrypted config file exists');
      return;
    }
    var files = fs.readdirSync(path.resolve(root, 'config'));
    files.forEach(function (item) {
      if (!item.match(/\.encrypted$/)) return;
      if (auto && item != 'default.json.encrypted' && item != process.env.NODE_ENV + '.json.encrypted') return;
      console.log('Decrypting ' + item + '...');
      var decipher = crypto.createDecipheriv('aes-256-ctr', _this.key.toString('hex').slice(0, 32), _this.key.toString('hex').slice(0, 16));
      var decrypted = decipher.update(fs.readFileSync(path.resolve(root, 'config', item), 'utf8'), 'hex', 'utf8');
      decrypted += decipher.final('utf8');
      fs.writeFileSync(
        path.resolve(root, 'config', item.replace('.encrypted', '')),
        decrypted
      );
    });
  };

  return _this;

};

# Traildock Node Common Library

To use this library in your node project, run:

```
yarn add https://bitbucket.org/gomaps-us/traildock-node-library
```
or with npm
```
npm install --save https://bitbucket.org/gomaps-us/traildock-node-library
```

And then require it in your project:

```
var traildockLib = require('traildock-node-library');
```

## Developing

### Dependencies for Developing

* Node >=9
* [Yarn](https://yarnpkg.com/en/docs/install). You can use npm if you like locally, but the preferred method is Yarn, and it's what we use for dependency installation and script execution in other environments.

### Testing

No tests written for this project yet

### Deploying

Just `git push`, this project isn't deployed to any registry.
